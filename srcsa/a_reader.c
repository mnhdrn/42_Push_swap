/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   a_reader.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:06:28 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:06:28 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static bool			other(char *line, t_data *data)
{
	if (ft_strequ(data->inst_name[8], line))
	{
		data->f[0](data);
		data->f[4](data);
		return (true);
	}
	else if (ft_strequ(data->inst_name[9], line))
	{
		data->f[2](data);
		data->f[6](data);
		return (true);
	}
	else if (ft_strequ(data->inst_name[10], line))
	{
		data->f[3](data);
		data->f[7](data);
		return (true);
	}
	else
		return (false);
}

static void			execute(char *line, t_data *data)
{
	int				i;

	i = 0;
	while (i < 8)
	{
		if (ft_strequ(data->inst_name[i], line))
		{
			data->f[i](data);
			return ;
		}
		i++;
	}
	if (other(line, data) == false)
		ft_message(2, data);
	return ;
}

bool				m_reader(t_data *data)
{
	int				rd;
	char			*line;

	line = NULL;
	(void)data;
	while ((rd = get_next_line(0, &line)) > 0)
	{
		execute(line, data);
		line ? free(line) : 0;
	}
	line ? free(line) : 0;
	if (ft_issort(data) == true)
		ft_message(0, data);
	else
		ft_message(1, data);
	return (true);
}
