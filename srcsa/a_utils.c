/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   a_utils.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:06:29 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:06:29 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

bool				ft_issort(t_data *data)
{
	int				i;

	i = 0;
	while (i < (int)data->len)
	{
		if (i > 0 && (!data->stka || data->stka[i - 1] > data->stka[i]))
			return (false);
		if (data->nb_b != 0)
			return (false);
		i++;
	}
	return (true);
}

void				ft_message(int ind, t_data *data)
{
	if (ind == 0)
		ft_putendl("OK");
	else if (ind == 1)
		ft_putendl("KO");
	else if (ind == 2)
		ft_putendl("Error");
	if (data)
	{
		if (data->stka != NULL)
			ft_int_tabdel(&data->stka);
		if (data->stkb != NULL)
			ft_int_tabdel(&data->stkb);
		if (data->inst_name != NULL)
			ft_str_tabdel(&data->inst_name);
	}
	exit(EXIT_SUCCESS);
}
