/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   a_inst_b.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:06:28 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:06:28 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void			inst_sb(t_data *data)
{
	int			tmp;
	int			posb;

	tmp = 0;
	posb = ((int)(data->len) - data->nb_b);
	if (data->len < 2 || data->nb_b < 2)
		return ;
	tmp = data->stkb[posb];
	data->stkb[posb] = data->stkb[posb + 1];
	data->stkb[posb + 1] = tmp;
}

void			inst_pb(t_data *data)
{
	int			posa;
	int			posb;

	posa = ((int)(data->len) - data->nb_a);
	posb = ((int)(data->len - 1) - data->nb_b);
	posa = (posa < 0) ? 0 : posa;
	posb = (posb < 0) ? 0 : posb;
	if (data->nb_a <= 0)
		return ;
	data->stkb[posb] = data->stka[posa];
	data->stka[posa] = 0;
	data->nb_a--;
	data->nb_b++;
}

void			inst_rb(t_data *data)
{
	int			i;
	int			tmp;
	int			posb;
	int			len;

	i = 0;
	posb = ((int)(data->len) - data->nb_b);
	len = (int)data->len - posb;
	if (data->nb_b < 1)
		return ;
	tmp = data->stkb[posb];
	while (i < len)
	{
		if (i < (len - 1))
			data->stkb[posb + i] = data->stkb[posb + (i + 1)];
		i++;
	}
	data->stkb[posb + (i - 1)] = tmp;
}

void			inst_rrb(t_data *data)
{
	int			i;
	int			posb;
	int			len;
	int			tmp;
	int			last;

	i = 0;
	posb = ((int)(data->len) - data->nb_b);
	len = (int)data->len - posb;
	last = posb + (len - 1);
	if (data->nb_b < 1)
		return ;
	tmp = data->stkb[posb + (len - 1)];
	while (i < len)
	{
		if (i < (len - 1))
			data->stkb[last - i] = data->stkb[last - (i + 1)];
		i++;
	}
	data->stkb[posb] = tmp;
}
