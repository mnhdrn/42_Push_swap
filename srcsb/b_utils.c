/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_utils.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:07:21 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:07:21 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int			ft_n_sort(int *tab, int n, int len)
{
	int		i;
	int		ret;
	int		pos;

	i = 0;
	ret = 0;
	pos = (len - n);
	while (i < (n - 1))
	{
		if (tab[pos + i] < tab[pos + (i + 1)])
			ret++;
		else
			return (ret > 0 ? ret + 1 : ret);
		i++;
	}
	return (ret > 0 ? ret + 1 : ret);
}

bool		ft_issort(int *tab, int n, int len, int sens)
{
	int		i;
	int		ret;
	int		pos;

	i = 0;
	ret = 1;
	pos = (len - n);
	while (i < (n - 1))
	{
		if (sens == 0)
		{
			if (tab[pos + i] < tab[pos + (i + 1)])
				ret++;
		}
		else
		{
			if (tab[pos + i] > tab[pos + (i + 1)])
				ret++;
		}
		i++;
	}
	return ((ret == len) ? true : false);
}

void		ft_largest(int *tab, int *large, int n, int len)
{
	int		i;
	int		pos;

	i = 0;
	if (n == 0)
		return ;
	pos = (len - n);
	(*large) = tab[pos];
	while (i < n)
	{
		if ((*large) < tab[pos + i])
			(*large) = tab[pos + i];
		i++;
	}
}

void		ft_smallest(int *tab, int *small, int n, int len)
{
	int		i;
	int		pos;

	i = 0;
	if (n == 0)
		return ;
	pos = (len - n);
	(*small) = tab[pos];
	while (i < n)
	{
		if ((*small) > tab[pos + i])
			(*small) = tab[pos + i];
		i++;
	}
}

void		ft_message(int ind, t_data *data)
{
	if (ind == 2)
		ft_putendl("Error");
	if (data)
	{
		if (data->stka != NULL)
			ft_int_tabdel(&data->stka);
		if (data->stkb != NULL)
			ft_int_tabdel(&data->stkb);
		if (data->stmp != NULL)
			ft_int_tabdel(&data->stmp);
	}
	(void)data;
	exit(EXIT_SUCCESS);
}
