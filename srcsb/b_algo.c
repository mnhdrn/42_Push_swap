/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_algo.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:07:19 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:07:19 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int				get_pos_small(t_data *data, int src, int len)
{
	int					i;
	int					posa;
	int					posb;

	i = 0;
	posa = ((int)(data->len) - data->nb_a);
	posb = ((int)(data->len) - data->nb_b);
	if (src == 1)
		ft_smallest(data->stka, &data->small, data->nb_a, (int)data->len);
	else
		ft_smallest(data->stkb, &data->small, data->nb_b, (int)data->len);
	while (i < len)
	{
		if (src == 1 && data->stka[posa + i] == data->small)
			return (i);
		else if (src != 1 && data->stkb[posb + i] == data->small)
			return (i);
		i++;
	}
	return (i);
}

static int				get_pos_large(t_data *data, int src, int len)
{
	int					i;
	int					posa;
	int					posb;

	i = 0;
	posa = ((int)(data->len) - data->nb_a);
	posb = ((int)(data->len) - data->nb_b);
	if (src == 1)
		ft_largest(data->stka, &data->large, data->nb_a, (int)data->len);
	else
		ft_largest(data->stkb, &data->large, data->nb_b, (int)data->len);
	while (i < len)
	{
		if (src == 1 && data->stka[posa + i] == data->large)
			return (i);
		else if (src != 1 && data->stkb[posb + i] == data->large)
			return (i);
		i++;
	}
	return (i);
}

static void				s_rewind(t_data *data, int src, int len)
{
	int					pos;
	int					posa;
	int					posb;

	pos = get_pos_small(data, src, len);
	posa = ((int)(data->len) - data->nb_a);
	posb = ((int)(data->len) - data->nb_b);
	while (true)
	{
		if (pos > (len / 2))
			(src == 1) ? inst_rra(data, 1) : inst_rrb(data, 1);
		else
			(src == 1) ? inst_ra(data, 1) : inst_rb(data, 1);
		if (src == 1 && data->stka[posa] == data->small)
			break ;
		else if (src != 1 && data->stkb[posb] == data->small)
			break ;
	}
}

static void				merge_it(t_data *data)
{
	int					pos;
	int					posb;

	pos = get_pos_large(data, 0, data->nb_b);
	ft_largest(data->stkb, &data->large, data->nb_b, (int)data->len);
	while (data->nb_b > 0)
	{
		posb = ((int)(data->len) - data->nb_b);
		if (data->stkb[posb] == data->large)
		{
			inst_pa(data, 1);
			pos = get_pos_large(data, 0, data->nb_b);
		}
		else if (pos > (data->nb_b / 2))
			inst_rrb(data, 1);
		else
			inst_rb(data, 1);
	}
}

void					m_algo(t_data *data)
{
	if (data->len < 2)
		return ;
	if (ft_n_sort(data->stka, data->nb_a, (int)data->len) == (int)data->len)
		return ;
	if (data->len < 6)
		al_tiny(data, (int)data->len);
	else
	{
		al_set_tmp(data);
		al_select(data);
		merge_it(data);
	}
	if (ft_n_sort(data->stka, data->nb_a, (int)data->len) < (int)data->len)
		s_rewind(data, 1, (int)data->len);
}
