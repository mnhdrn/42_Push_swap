/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_process.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:07:20 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:07:20 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static bool				check(int *tab, size_t len)
{
	int					tmp;
	size_t				i;
	size_t				j;

	i = 0;
	j = 0;
	tmp = 0;
	while (i < len)
	{
		tmp = tab[i];
		j = 0;
		while (j < len)
		{
			if (j > 0 && j != i && tab[j] == tmp)
				return (false);
			j++;
		}
		i++;
	}
	return (true);
}

static bool				valid(char *s)
{
	int					i;
	int					j;
	char				tmp[100];
	char				*tmp_i;

	i = 0;
	j = 0;
	if (ft_strequ("", s))
		return (false);
	j = ft_atoi(s);
	tmp_i = ft_itoa(j);
	ft_strcpy(tmp, tmp_i);
	ft_strdel(&tmp_i);
	while (s[i])
	{
		if (!(ft_isdigit(s[i]) == 1 || ft_strnchr("+- ", s[i]) > 0))
			return (false);
		i++;
	}
	if (ft_strnchr(s, '-') != ft_strnchr(tmp, '-'))
		return (false);
	return (true);
}

static void				stock(int ac, char **av, t_data *data)
{
	int					i;
	int					j;
	char				**tab;

	i = 1;
	j = 0;
	tab = NULL;
	while (i < ac && av[i])
	{
		tab = ft_strsplit(av[i], ' ');
		j = 0;
		while (tab[j])
		{
			if (i > 0 && valid(tab[j]) == true && tab[j])
				data->stka[(i - 1) + j] = ft_atoi(tab[j]);
			else
				ft_message(2, data);
			j++;
		}
		(tab != NULL) ? ft_str_tabdel(&tab) : 0;
		i++;
	}
	(tab != NULL) ? ft_str_tabdel(&tab) : 0;
	if (check(data->stka, data->len) == false)
		ft_message(2, data);
}

static inline void		clean(int ac, char **av, t_data *data)
{
	int					i;
	int					j;
	size_t				tablen;

	i = 1;
	j = 0;
	tablen = 0;
	while (i < ac)
	{
		tablen += ft_countword(av[i], ' ');
		j = 0;
		while (av[i][j])
		{
			av[i][j] = (av[i][j] == 39 || av[i][j] == 34) ? ' ' : av[i][j];
			j++;
		}
		i++;
	}
	data->len = tablen;
	data->nb_a = (int)tablen;
}

void					m_process(int ac, char **av, t_data *data)
{
	clean(ac, av, data);
	if (!(data->stka = ft_int_tabmake(data->len)))
		ft_message(2, data);
	if (!(data->stkb = ft_int_tabmake(data->len)))
		ft_message(2, data);
	if (!(data->stmp = ft_int_tabmake(data->len)))
		ft_message(2, data);
	stock(ac, av, data);
}
