/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_tmp.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 13:07:21 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:07:21 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void			init_it(t_data *data)
{
	int				rng;

	rng = RANGE;
	data->mid = (int)data->len / 2;
	data->per = ((int)data->len) / rng;
	data->per = (rng > (int)data->len) ? ((int)data->len - data->mid) \
				: data->per;
	data->cnt[0] = data->mid;
	data->cnt[1] = data->mid - data->per;
	data->cnt[2] = data->mid;
	data->cnt[3] = data->mid + data->per;
}

static void			sort_it(int **arr, int n)
{
	int			i;
	int			j;
	int			key;

	i = 1;
	j = 0;
	key = 0;
	while (i < n)
	{
		key = (*arr)[i];
		j = i - 1;
		i++;
		while (j >= 0 && (*arr)[j] > key)
		{
			(*arr)[j + 1] = (*arr)[j];
			j = j - 1;
		}
		(*arr)[j + 1] = key;
	}
}

static void			copy_it(int **src, int **dest, size_t len)
{
	size_t			i;

	i = 0;
	while (i < len)
	{
		(*dest)[i] = (*src)[i];
		i++;
	}
}

void				al_set_tmp(t_data *data)
{
	copy_it(&data->stka, &data->stmp, data->len);
	sort_it(&data->stmp, (int)data->len);
	init_it(data);
}
