/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/02 11:12:18 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 12:56:21 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft.h"
# include <stdarg.h>
# include <stdlib.h>
# include <stdint.h>
# include <stdbool.h>
# include <unistd.h>
# include <limits.h>

# define COLOR_RED     "\x1b[31m"
# define COLOR_GREEN   "\x1b[32m"
# define COLOR_YELLOW  "\x1b[33m"
# define COLOR_BLUE    "\x1b[34m"
# define COLOR_MAGENTA "\x1b[35m"
# define COLOR_CYAN    "\x1b[36m"
# define COLOR_RESET   "\x1b[0m"

# define BUFFER			512

typedef union			u_value
{
	long long			tint;
	unsigned long		taddr;
	unsigned long long	tuint;
}						t_value;

typedef struct			s_flag
{
	t_value				value;
	bool				(*f[13])(char *s);
	unsigned char		modifier;
	long				v_sign;
	long				v_size;
	long				v_padding;
	long				len;
	char				*s_sign;
	char				*s_size;
	char				*s_padding;
	char				*s_flag;
}						t_flag;

typedef struct			s_dna
{
	t_flag				flag;
	va_list				va;
	char				buffer[BUFFER + 1];
	size_t				format_len;
	size_t				stocked;
	size_t				index;
	int					ret;
}						t_dna;

int						ft_printf(const char *format, ...);
void					ft_parse(const char *format);
bool					ft_color(const char *format);
void					ft_flag(char *s, t_dna *dna);
void					ft_buffer(char *s, size_t len);
bool					is_type(char *s);
bool					is_valid(char *s);
bool					is_int(char *s);
bool					is_signed(char *s);
bool					is_char(char *s);
bool					is_addr(char *s);

bool					ex_other(char *s);
bool					ex_int(char *s);
bool					ex_uint(char *s);
bool					ex_char(char *s);
bool					ex_string(char *s);
bool					ex_addr(char *s);
bool					ex_percent(char *s);

void					get_modifier(char *s, t_dna *dna);
void					get_precision(char *s, t_dna *dna);
void					set_flag(t_dna *dna);
char					*set_sign(int index);
char					*set_size(void);
char					*set_padding(void);

char					*unicode(wchar_t c);
t_dna					*call(void);

#endif
