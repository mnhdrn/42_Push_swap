/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 17:15:10 by clrichar          #+#    #+#             */
/*   Updated: 2018/05/17 13:18:05 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <stdlib.h>
# include <unistd.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <fcntl.h>
# include <string.h>
# include <wchar.h>
# include "ft_printf.h"
# define BUFF_SIZE 320

typedef struct		s_list {
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

void				*ft_memset(void *s, int c, size_t n);
void				*ft_memcpy(void *dest, const void *src, size_t n);
void				*ft_memccpy(void *dest, const void *src, int c, size_t n);
void				*ft_memmove(void *dest, const void *src, size_t n);
void				*ft_memchr(const void *s, int c, size_t n);
void				*ft_memalloc(size_t size);
void				ft_bzero(void *s, size_t n);
void				ft_memdel(void **ap);
int					ft_memcmp(const void *s1, const void *s2, size_t n);

char				*ft_strdup(const char *s);
char				*ft_strcpy(char *dest, const char *src);
char				*ft_strncpy(char *dest, const char *src, size_t n);
char				*ft_strcat(char *dest, const char *src);
char				*ft_strncat(char *dest, const char *src, size_t n);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strstr(const char *haystack, const char *needle);
char				*ft_strnstr(const char *big, const char *sml, size_t len);
char				*ft_strnew(size_t size);
char				*ft_strmap(char const *s, char (*f)(char));
char				*ft_strmapi(char const *s, char (*f)(unsigned int, char));
char				*ft_strtrim(char const *s, char c);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strsub_free(char *s, unsigned int start, size_t len);
char				*ft_strjoin(const char *s1, const char *s2);
char				*ft_strjoin_free(const char *s1, const char *s2, int index);
char				**ft_strsplit(char const *s, char c);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
void				ft_striter(char *s, void (*f)(char *));
void				ft_striteri(char *s, void (*f)(unsigned int, char *));
void				ft_strrev(char *s);
void				ft_str_tolower(char **str);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
int					ft_strnchr(const char *s, int c);

void				ft_wstrdel(wchar_t **as);
size_t				ft_wstrlen(const wchar_t *ws);
size_t				ft_wputchar(wint_t c);
size_t				ft_wputstr(wchar_t *s);
wchar_t				*ft_wstrnew(size_t size);
wchar_t				*ft_wmerge(wchar_t *ws, char *s);
wchar_t				*ft_wstrcat(wchar_t *dest, const wchar_t *src);
wchar_t				*ft_wstrdup(const wchar_t *ws);
wchar_t				*ft_wstrjoin(const wchar_t *s1, const wchar_t *s2);
wchar_t				*ft_wstrjoin_free(const wchar_t *s1, const wchar_t *s2,
		int index);

int					ft_isascii(int c);
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isprint(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);
int					ft_isspace(char c);

char				*ft_itoa(int n);
char				*ft_itoa_base(long long n, long long base);
char				*ft_utoa_base(unsigned long long n, long long base);
int					ft_atoi(const char *nptr);
int					ft_atoi_base(const char *str, size_t base);

void				ft_putchar(char c);
void				ft_putstr(char const *s);
void				ft_putendl(char const *s);
void				ft_putnbr(int n);
void				ft_str_puttab(char **tab);
void				ft_int_puttab(int *tab, size_t size);

void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);

size_t				ft_intlen(int n);
size_t				ft_strlen(const char *s);
int					ft_strlenc(const char *s, char c);
size_t				ft_countword(char const *s, char c);

int					*ft_int_tabmake(size_t size);
void				ft_int_tabdel(int **as);

char				**ft_str_tabmake(size_t size);
void				ft_str_tabdel(char ***as);
char				*ft_swap_char(char *str, int start, int end);
char				**ft_swap_tab(char **tab, int start, int end);

int					ft_sqrt(int nb);
size_t				ft_factorial(size_t nb);
void				ft_printbits(unsigned char octet);

int					get_next_line(const int fd, char **line);

#endif
