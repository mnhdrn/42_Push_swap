/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 14:37:27 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/10 21:05:52 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

#define POS dna->index
#define FLAGS "ABCEDEFGHIJKLMNOPQRSTUVWXYZabcdefgikmnopqrstuvwxy%"

static bool			p_color(const char *format, t_dna *dna)
{
	size_t			i;
	char			*s;

	i = 0;
	s = NULL;
	if (format[POS] && (format[POS + i] == '}' || \
				format[POS + i] == '%'))
		return (false);
	while (format[POS + i] && format[POS + i] != '}')
		i++;
	if (i > 0)
	{
		i++;
		s = ft_strsub(format, (unsigned int)POS, i);
		if (ft_color(s) == false)
		{
			ft_strdel(&s);
			POS = POS + 1;
			return (false);
		}
		ft_strdel(&s);
		POS = POS + i;
		return (true);
	}
	return (false);
}

static bool			p_string(const char *format, t_dna *dna)
{
	char			*s;
	size_t			i;

	i = 0;
	s = NULL;
	if (format[dna->index] && (format[dna->index + i] == '%' || \
				format[POS + i] == '{'))
		return (false);
	while (format[POS + i] && (format[POS + i] != '%' && \
				format[POS + i] != '{'))
		i++;
	if (i > 0)
	{
		s = ft_strsub(format, (unsigned int)POS, i);
		ft_buffer(s, i);
		ft_strdel(&s);
		POS = POS + i;
		return (true);
	}
	return (false);
}

static bool			p_flag(const char *format, t_dna *dna)
{
	size_t			i;
	char			*s;

	i = 1;
	s = NULL;
	while (format[POS + i] && ft_strchr(FLAGS, format[POS + i]) == 0)
		i++;
	i++;
	if (i > 0)
	{
		s = ft_strsub(format, (unsigned int)POS, i);
		ft_flag(s, dna);
		ft_strdel(&s);
		POS = POS + i;
		return (true);
	}
	return (false);
}

void				ft_parse(const char *format)
{
	bool			ret;
	t_dna			*dna;

	ret = true;
	dna = call();
	if ((ret = p_string(format, dna)) == false)
	{
		if ((ret = p_color(format, dna)) == false)
		{
			if ((ret = p_flag(format, dna)) == false)
				return (ft_parse(format));
		}
	}
	if (POS < dna->format_len)
		return (ft_parse(format));
}
