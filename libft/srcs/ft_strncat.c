/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:45 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:45 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char				*ft_strncat(char *dest, const char *src, size_t n)
{
	size_t			i;
	size_t			len;

	i = 0;
	len = ft_strlen(dest);
	while (i < n && *(src + i))
	{
		*(dest + (len + i)) = *(src + i);
		i++;
	}
	*(dest + (len + i)) = '\0';
	return (dest);
}
