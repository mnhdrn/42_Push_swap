/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wputchar.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:49 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/10 22:38:29 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t			ft_print_one(unsigned char *const buffer, wint_t c)
{
	buffer[0] = (unsigned char)c;
	buffer[1] = '\0';
	ft_putstr((const char *)buffer);
	return (1);
}

static size_t			ft_print_two(unsigned char *const buffer, wint_t c)
{
	buffer[0] = 0xC0 | (unsigned char)(c >> 6);
	buffer[1] = 0x80 | (unsigned char)(c & 0x3F);
	buffer[2] = '\0';
	ft_putstr((const char *)buffer);
	return (2);
}

static size_t			ft_print_three(unsigned char *const buffer, wint_t c)
{
	buffer[0] = 0xE0 | (unsigned char)(c >> 12);
	buffer[1] = 0x80 | (unsigned char)((c >> 6) & 0x3F);
	buffer[2] = 0x80 | (unsigned char)(c & 0x3F);
	buffer[3] = '\0';
	ft_putstr((const char *)buffer);
	return (3);
}

static size_t			ft_print_four(unsigned char *const buffer, wint_t c)
{
	buffer[0] = 0xF0 | (unsigned char)(c >> 18);
	buffer[1] = 0x80 | (unsigned char)((c >> 12) & 0x3F);
	buffer[2] = 0x80 | (unsigned char)((c >> 6) & 0x3F);
	buffer[3] = 0x80 | (unsigned char)(c & 0x3F);
	buffer[4] = '\0';
	ft_putstr((const char *)buffer);
	return (4);
}

size_t					ft_wputchar(wint_t c)
{
	unsigned char		buffer[5];

	if (c <= 0x7F)
		return (ft_print_one(buffer, c));
	if (c <= 0x7FF)
		return (ft_print_two(buffer, c));
	if (c <= 0xFFFF)
		return (ft_print_three(buffer, c));
	if (c <= 0x10FFFF)
		return (ft_print_four(buffer, c));
	return (0);
}
