/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:47 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:47 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int				ft_getstart(const char *s, char c, int n)
{
	int					i;
	int					j;

	i = 0;
	j = 0;
	n += 1;
	if (*(s + 0) != c && n == 1)
		return (0);
	else if (*(s + 0) != c)
		n--;
	i = 1;
	while (*(s + i) && j < n)
	{
		if (*(s + i) != c && *(s + (i - 1)) == c)
			j++;
		i++;
	}
	return (i - 1);
}

static int				ft_getend(char const *s, char c, int start)
{
	int					i;

	i = 0;
	while (*(s + (start + i)) != c && *(s + (start + i)) != '\0')
		i++;
	return (i);
}

char					**ft_strsplit(char const *s, char c)
{
	char		**ret;
	size_t		i;
	int			start;
	int			end;

	if (!s || !c)
		return (NULL);
	if (!(ret = (char**)malloc(sizeof(char*) * (ft_countword(s, c) + 1))))
		return (NULL);
	i = 0;
	while (i < ft_countword(s, c))
	{
		start = ft_getstart(s, c, i);
		end = ft_getend(s, c, start);
		ret[i] = ft_strsub(s, start, end);
		i++;
	}
	ret[i] = NULL;
	return (ret);
}
