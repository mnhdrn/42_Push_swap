# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: clrichar <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/02/20 15:08:30 by clrichar          #+#    #+#              #
#    Updated: 2018/05/17 13:42:47 by clrichar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAMEA			:=			checker
NAMEB			:=			push_swap

#==============================================================================#
#------------------------------------------------------------------------------#
#                               DIRECTORIES                                    #

SRC_DIRA			:=			./srcsa
SRC_DIRB			:=			./srcsb
INC_DIR				:=			./includes
OBJ_DIR				:=			./obj
LIB_DIR				:=			./libft

#==============================================================================#
#------------------------------------------------------------------------------#
#                                  FILES                                       #

SRCA				:=			checker.c			\
								a_process.c			\
								a_reader.c			\
								a_inst_a.c			\
								a_inst_b.c			\
								a_utils.c			\

SRCB				:=			push_swap.c			\
								b_process.c			\
								b_algo.c			\
								b_inst_a.c			\
								b_inst_b.c			\
								b_inst_c.c			\
								b_tmp.c				\
								b_utils.c			\
								sort_tiny.c			\
								sort_select.c		\
								merge_final.c		\

OBJA				:=			$(addprefix $(OBJ_DIR)/,$(SRCA:.c=.o))
OBJB				:=			$(addprefix $(OBJ_DIR)/,$(SRCB:.c=.o))
NBA					:=			$(words $(SRCA))
NBB					:=			$(words $(SRCB))
INDEXA				:=			0
INDEXB				:=			0

#==============================================================================#
#------------------------------------------------------------------------------#
#                            COMPILER & FLAGS                                  #

CC					:=			gcc
CFLAGS				:=			-Wall -Wextra -Werror
OFLAGS				:=			-pipe
CFLAGS				+=			$(OFLAGS)
CLIB				:=			-L$(LIB_DIR) -lft

#==============================================================================#
#------------------------------------------------------------------------------#
#                                LIBRARY                                       #

L_FT				:=			$(LIB_DIR)

#==============================================================================#
#------------------------------------------------------------------------------#
#                                 RULES                                        #

all:					lib obj_inc $(NAMEA) $(NAMEB)


$(NAMEA):				$(OBJA)
	@$(CC) $(OFLAGS) $(OBJA) $(CLIB) -o $(NAMEA)
	@printf '\033[33m[ 100%% ] %s\n\033[0m' "Compilation of $(NAMEA) is done ---"


$(NAMEB):				$(OBJB)
	@$(CC) $(OFLAGS) $(OBJB) $(CLIB) -o $(NAMEB)
	@printf '\033[33m[ 100%% ] %s\n\033[0m' "Compilation of $(NAMEB) is done ---"


$(OBJ_DIR)/%.o:			$(SRC_DIRA)/%.c
	@$(eval DONE=$(shell echo $$(($(INDEXA)*20/$(NBA)))))
	@$(eval PERCENT=$(shell echo $$(($(INDEXA)*100/$(NBA)))))
	@$(eval TO_DO=$(shell echo "$@"))
	@$(CC) $(CFLAGS) -I$(INC_DIR) -o $@ -c $<
	@printf "[ %d%% ] %s :: %s        \r" $(PERCENT) $(NAMEA) $@
	@$(eval INDEX=$(shell echo $$(($(INDEXA)+1))))


$(OBJ_DIR)/%.o:			$(SRC_DIRB)/%.c
	@$(eval DONE=$(shell echo $$(($(INDEXB)*20/$(NBB)))))
	@$(eval PERCENT=$(shell echo $$(($(INDEXB)*100/$(NBB)))))
	@$(eval TO_DO=$(shell echo "$@"))
	@$(CC) $(CFLAGS) -I$(INC_DIR) -o $@ -c $<
	@printf "[ %d%% ] %s :: %s        \r" $(PERCENT) $(NAMEB) $@
	@$(eval INDEX=$(shell echo $$(($(INDEXB)+1))))


obj_inc:
	@mkdir -p $(OBJ_DIR)


lib:
	@make -C $(L_FT) --no-print-directory


clean:
	@make -C $(L_FT) clean --no-print-directory  
	@rm -rf $(OBJ_DIR)
	@printf '\033[33m[ KILL ] %s\n\033[0m' "Clean of $(NAME) is done ---"


fclean: 				clean
	@rm -rf $(NAMEA)
	@rm -rf $(NAMEB)
	@make -C $(L_FT) fclean --no-print-directory
	@printf '\033[33m[ KILL ] %s\n\033[0m' "Fclean of $(NAME) is done ---"


re:						fclean all


.PHONY: all clean fclean re build cbuild
